(function(){
	'use strict';

	angular.module('PORTAL.detailedSearch', [])
		
		.filter('startFrom', function() {
			    return function(input, start) {
			        if (!input || !input.length) { return; }
			        start = +start; //parse to int
			        return input.slice(start);
			    }
		})

		.filter('unique', function() {
		    return function (arr, field) {
		        return _.uniq(arr, function(a) { return a[field]; });
		    };
		});

})()