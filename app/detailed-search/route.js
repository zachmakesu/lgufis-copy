(function(){
  'use strict';

  angular.module('PORTAL.detailedSearch')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider

    .state("detailed-search", {
      url: "/detailed-search",
      templateUrl: "app/detailed-search/index.html",
      controller: "DetailedSearchCtrl"
    })


  }

})()