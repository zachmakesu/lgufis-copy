(function(){
  'use strict';

  angular.module('PORTAL.detailedSearch')
  .controller('DetailedSearchCtrl',['$scope','$rootScope','GlobalData','$stateParams','$localStorage','$state','FetchData','uiGmapGoogleMapApi','$filter',controllerFunc])


  function controllerFunc($scope,$rootScope,GlobalData,$stateParams,$localStorage,$state,FetchData,uiGmapGoogleMapApi,$filter){

    var vm = $scope;


    vm.lguFilter = [];

    FetchData.get('/lgu_names/all')
    .then(
      function(success){
        vm.multipleLgus = success.data.data;
        console.log(vm.multipleLgus);
        console.log(vm.multipleLgus[0]);

        vm.currentPage = 0;
        vm.pageSize = 10;


        var lguType = [];
        var regions = [];
        var incomeClass = [];
        vm.lguType = lguType;
        vm.regions = regions;
        vm.incomeClass = incomeClass;

        
        for (var i = 0; i < vm.multipleLgus.length; i++) {
          // vm.testingFilter.push(testFilter);
          if (vm.multipleLgus != null) {
            var pushRegions = vm.multipleLgus[i].region;
            var pushClass = vm.multipleLgus[i].income_class;
            if (pushRegions != null) {
              regions.push(pushRegions.toLowerCase());
            }
            if (pushClass != null) {
              incomeClass.push(pushClass.toLowerCase());
            }
            lguType.push(vm.multipleLgus[i].lgu_type.toLowerCase());
          }
        }

        var lguCleaned = [];

        for (var i = 0; i < vm.multipleLgus.length; i++) {
          var lguSingle = {
            "id": "",
            "name": "",
            "income_class": "",
            "type": "",
            "region": ""
          }
          lguSingle.name = vm.multipleLgus[i].name;
          lguSingle.income_class = vm.multipleLgus[i].income_class;
          lguSingle.type = vm.multipleLgus[i].lgu_type.toLowerCase();
          lguSingle.id = vm.multipleLgus[i].id;
          if (vm.multipleLgus[i].region != null) {
            lguSingle.region = vm.multipleLgus[i].region.toLowerCase();
          }
          lguCleaned.push(lguSingle);
        }

        console.log("cleaned",lguCleaned);

        var detailedFilter = $filter('filter')(lguCleaned, {type:'', income_class:'', region:''});
        vm.detailedFilter = detailedFilter;

        vm.numberOfPages=function(){
            return Math.ceil(vm.detailedFilter.length/vm.pageSize);                
        }

        vm.typeChange = function(lguType) {
          vm.filterType = lguType.toLowerCase();
          var detailedFilter = $filter('filter')(lguCleaned, {type:vm.filterType, income_class:vm.filterClass, region:vm.filterRegion},true);
          vm.detailedFilter = detailedFilter;
          // vm.numberOfPages=function(){
          //     return Math.ceil(vm.detailedFilter.length/vm.pageSize);                
          // }
        }

        vm.classChange = function(lguClass) {
          vm.filterClass = lguClass;
          var detailedFilter = $filter('filter')(lguCleaned, {type:vm.filterType, income_class:vm.filterClass, region:vm.filterRegion});
          vm.detailedFilter = detailedFilter;
        }

        vm.regionChange = function(lguRegion) {
          vm.filterRegion = lguRegion.toLowerCase();
          var detailedFilter = $filter('filter')(lguCleaned, {type:vm.filterType, income_class:vm.filterClass, region:vm.filterRegion},true);
          vm.detailedFilter = detailedFilter;
        }


      },
      function(error){
        console.log('error',error);
      }
    );    

    vm.incomeFilter = function(lgu){
        return lgu.income_class.toLowerCase() === 'new' || 
        lgu.income_class.toLowerCase() === 'nc' || 
        lgu.income_class.toLowerCase() == 'special' ||
        lgu.income_class.toLowerCase() == '1st (as municipality)';
    };


      angular.element(document).ready(function () {
          $(".top-bar").addClass("animated visible slideInDown");
      });

  }
})()