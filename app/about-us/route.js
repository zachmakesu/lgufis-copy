(function(){
  'use strict';

  angular.module('PORTAL.aboutUs')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider

    .state("about-us", {
      url: "/about-us",
      templateUrl: "app/about-us/index.html",
      controller: "AboutUsCtrl",
      title: 'About Us'
    })


  }

})()