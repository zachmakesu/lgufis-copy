(function(){
  'use strict';

  angular.module('PORTAL.home')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider

    .state("home", {
      url: "/",
      templateUrl: "app/home/index.html",
      controller: "HomeCtrl",
      title: 'Home'
    })


  }

})()