(function(){
	'use strict';

  angular.module('PORTAL', [
  	'ui.router',
  	'ngStorage',
  	'chart.js',
  	'angular-svg-round-progressbar',
  	'PORTAL.core'
  ])

})()