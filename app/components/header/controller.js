(function(){
	'use strict';

  angular.module('PORTAL.components')
  .controller('Header',['$scope','$stateParams','$state',controllerFunc])

	function controllerFunc($scope,$stateParams,$state){
		var vm = $scope;
		angular.element(document).ready(function () {
	          $(".top-bar").addClass("animated visible slideInDown");
	      });

		if ($state.current.name != 'home') {
			vm.notHome = true;
		} else {
			vm.notHome = false;
		}
	}

})()