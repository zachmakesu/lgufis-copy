(function(){
	'use strict';

	angular.module('PORTAL.components')

	.directive('header', 	function() {
		return {
			restrict: 'A',
			templateUrl: 'app/components/header/index.html',
			controller: 'Header'
		}
	});

})()