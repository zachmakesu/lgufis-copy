(function(){
	'use strict';

	angular.module('PORTAL.components')

	.directive('banner', 	function() {
		return {
			restrict: 'A',
			templateUrl: 'app/components/banner/index.html',
			controller: 'Banner'
		}
	});

})()