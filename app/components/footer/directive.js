(function(){
	'use strict';

	angular.module('PORTAL.components')

	.directive('footer', 	function() {
		return {
			restrict: 'A',
			templateUrl: 'app/components/footer/index.html'
		}
	});

})()