(function(){
	'use strict';

	angular.module('PORTAL.components')

	.directive('mainNavigation',['$location','$sessionStorage','$localStorage',directiveFunc]);

    function directiveFunc($location,$sessionStorage,$localStorage) {
        return {
            restrict: 'A',
            scope: true,
            link: function(scope, elem, attrs) {

                scope.$watch("$routeChangeSuccess", function () {
                    var hrefs = ['/#' + $location.path(),
                                 '#' + $location.path(), //html5: false
                                 $location.path()]; //html5: true
                    angular.forEach(elem.find('a'), function (a) {
                        a = angular.element(a);
                        if (-1 !== hrefs.indexOf(a.attr('href'))) {
                            a.parent().addClass('active');
                            if (!$localStorage.collapse) {
                                a.parent().parent().show();
                            }
                            a.parent().parent().parent().addClass('active');

                        } else {

                            a.parent().removeClass('active');
                        };
                    });
                });

            },
            templateUrl: 'app/components/main-navigation/index.html'
        }
    }

})()