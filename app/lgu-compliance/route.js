(function(){
  'use strict';

  angular.module('PORTAL.lguCompliance')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider
    
    .state("lgu-compliance", {
      url: "/search-lgu/:id/compliance",
      templateUrl: "app/lgu-compliance/index.html",
      controller: "LguCompliance"
    });


  }

})()