(function(){
  'use strict';

  angular.module('PORTAL.lguRevenue')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider
    
    .state("lgu-revenue", {
      url: "/search-lgu/:id/revenue",
      templateUrl: "app/lgu-revenue/index.html",
      controller: "LguRevenue"
    });


  }

})()