(function(){
  'use strict';

  angular.module('PORTAL.not-found')

  .config(['$stateProvider','LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider
    .state('not-found', {
      url: LOCATION + "/not-found",
      templateUrl: "app/not-found/index.html"
    })

  }

})()