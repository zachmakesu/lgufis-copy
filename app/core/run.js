(function(){
  angular.module('PORTAL.core')

  .run(['CheckApi','$rootScope','$localStorage','FetchData',runFunc])

  .config(function(uiGmapGoogleMapApiProvider) {
      uiGmapGoogleMapApiProvider.configure({
          key: 'AIzaSyAlvHglm9dUjmjnyfwnTHpI4piXQZHRxAE',
          v: '3.25', //defaults to latest 3.X anyhow
          libraries: 'weather,geometry,visualization'
      });
  })

  function runFunc(CheckApi,$rootScope,$localStorage,FetchData){
  	// CheckApi.status();

  	$rootScope.$on('$viewContentLoaded', function () {
	  	if ($localStorage.collapse) {
	  		$('.wrapper').addClass('collapse');
	  	}
    });

    // console.log($localStorage.lgusCoords);

    if ($localStorage.lgusCoords) {
      // console.log('is saved');
      $rootScope.lgus = $localStorage.lgusCoords;
    } else {
      console.log('not saved');
      FetchData.get('/lgu_names/all')
      .then(
        function(success){
          $rootScope.lgus = success.data.data;
          $localStorage.lgusCoords = success.data.data;
          // console.log(success.data.data);
        },
        function(error){
          console.log('error',error);
        }
      );


    }





  }

})()
