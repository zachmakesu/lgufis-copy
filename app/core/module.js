(function(){
	'use strict';

  angular.module('PORTAL.core', [

		// Providers
  	'PORTAL.configs',

    // Directives
    'PORTAL.directives',

  	// Services
		'PORTAL.services',

    // Components
    'PORTAL.components',

    // Google Map
    'uiGmapgoogle-maps',

    // Widgets
    'PORTAL.widgets',

  	// Modules
    'PORTAL.not-found',
  	'PORTAL.apierror',
  	'PORTAL.home',
    'PORTAL.aboutUs',
    'PORTAL.contactUs',
    'PORTAL.searchLgu',
    'PORTAL.searchLguSingle',
    'PORTAL.lguRevenue',
    'PORTAL.lguExpenditures',
    'PORTAL.lguCompliance',
    'PORTAL.searchIndicator',
    'PORTAL.detailedSearch',
    'PORTAL.eventDetails',
    'PORTAL.page2sample'

  ])
})()