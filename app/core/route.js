(function(){
  angular.module('PORTAL.core')

  .config(['$stateProvider', '$urlRouterProvider','$locationProvider', 'LOCATION',routeFunc]);

  function routeFunc($stateProvider, $urlRouterProvider,$locationProvider, LOCATION){

    // Unmatch url route to 404 page
    $urlRouterProvider.otherwise("/not-found");

    $locationProvider.html5Mode(true);

  }

})()
