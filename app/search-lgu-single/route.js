(function(){
  'use strict';

  angular.module('PORTAL.searchLguSingle')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider

    .state("search-lgu-single", {
      url: "/search-lgu/:id",
      templateUrl: "app/search-lgu-single/index.html",
      controller: "SearchLguSingleCtrl"
    })

  }

})()