(function(){
	'use strict';

  angular.module('PORTAL.searchLguSingle')
  .controller('SearchLguSingleCtrl',['$scope','$rootScope','$state','$stateParams','$localStorage','FetchData','uiGmapGoogleMapApi',controllerFunc])

  Array.prototype.filterObjects = function(key, value) {
    return this.filter(function(x) { return x[key] === value; })
  }

	function controllerFunc($scope,$rootScope,$state,$stateParams,$localStorage,FetchData,uiGmapGoogleMapApi){
		var vm = $scope;

    var current = parseInt($stateParams.id);
    vm.current = current;

    var lgus = $localStorage.lgusCoords;
    var curLgu = lgus.find(x => x.id === current);

    vm.lgus = lgus;
    
    vm.currentMarker = curLgu.coords; 
    vm.imageMarker = '/images/map-marker-'+curLgu.lgu_type.toLowerCase()+'.png';



    FetchData.get('/lgu_names/'+current,'|id='+current)
      .then(
        function(success){
            vm.currentData = success.data.data;
        },
        function(error){
          console.log('error:',error);
        }
      ); 

      var indicatorId = 2;
      
    FetchData.get('/lgu_names/'+current+'/indicator/'+indicatorId,'|id='+current+'|indicator_id='+indicatorId)
      .then(
        function(success){
            vm.chartData = success.data.data;
            console.log(vm.chartData);
        },
        function(error){
          console.log('error:',error);
        }
      );  

      
      // console.log($rootScope.lgus);


    vm.mapMarker = function(model) {
      return '/images/map-marker-'+model['lgu_type']+'.png';
    };

    vm.options = {
      scrollwheel: false
    }; 



      angular.element(document).ready(function () {
          $(".top-bar").addClass("animated visible slideInDown");
          setTimeout('$(".angular-google-map").addClass("animated visible slideInDown")', 800);
          setTimeout('$(".search-section").addClass("animated visible slideInLeft")', 1400);
          setTimeout('$(".search-section").removeClass("animated visible slideInLeft hidden")', 2400);
          setTimeout('$(".lgu-name-section").addClass("animated visible slideInUp")', 1500);
          setTimeout("$('#show-hide-3 .fa-angle-left').addClass('animated infinite slideInRight')", 3400)
      });

      $("#show-hide-3").click(function() {
        $('.search-section').toggleClass('search-close');
        $('#show-hide-3 .fa-angle-right').toggleClass('slideInLeft');
        $('#show-hide-3 .fa-angle-left').toggleClass('slideInRight'); 
      });

     };

})()