(function(){
  'use strict';

  angular.module('PORTAL.searchLgu')
  .controller('SearchLguCtrl',['$scope','$rootScope','GlobalData','$stateParams','$localStorage','$state','FetchData','uiGmapGoogleMapApi',controllerFunc])


  function controllerFunc($scope,$rootScope,GlobalData,$stateParams,$localStorage,$state,FetchData,uiGmapGoogleMapApi){

    var vm = $scope;


    vm.lguFilter = [];

    vm.mapMarker = function(model) {
      return '/images/map-marker-'+model['lgu_type'].toLowerCase()+'.png';
    }

    FetchData.get('/lgu_names/all')
    .then(
      function(success){
        vm.multipleLgus = success.data.data;
        // console.log(success.data.data);
        console.log(vm.multipleLgus[0]);
        console.log(vm.mapMarker(vm.multipleLgus[0]));
      },
      function(error){
        console.log('error',error);
      }
    );    

    vm.incomeFilter = function(lgu){
        return lgu.income_class.toLowerCase() === 'new' || 
        lgu.income_class.toLowerCase() === 'nc' || 
        lgu.income_class.toLowerCase() == 'special' ||
        lgu.income_class.toLowerCase() == '1st (as municipality)';
    };


    vm.map  = {
      "center": {
        "latitude": 11.6736111,
        "longitude": 118.1259741
      },
      zoom: 6,
    }

      angular.element(document).ready(function () {
          $(".top-bar").addClass("animated visible slideInDown");
          setTimeout('$(".angular-google-map").addClass("animated visible slideInDown")', 800);
          setTimeout('$(".search-section").addClass("animated visible slideInLeft")', 1400);
          setTimeout('$(".search-section").removeClass("animated visible slideInLeft hidden")', 2400);
          setTimeout("$('#show-hide-2 .fa-angle-left').addClass('animated infinite slideInRight')", 3400)
      });

      $("#show-hide-2").click(function() {
        $('.search-section').toggleClass('search-close');
        $('#show-hide-2 .fa-angle-right').toggleClass('slideInLeft');
        $('#show-hide-2 .fa-angle-left').toggleClass('slideInRight'); 
      });


    // console.log($scope.schedules);

    // console.log({data1: GlobalData.guests});
    // console.log({data2: GlobalData.schedule});

  }
})()