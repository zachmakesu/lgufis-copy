(function(){
  'use strict';

  angular.module('PORTAL.searchLgu')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider

    .state("search-lgu", {
      url: "/search-lgu",
      templateUrl: "app/search-lgu/index.html",
      controller: "SearchLguCtrl"
    })


  }

})()