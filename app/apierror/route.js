(function(){
  'use strict';

  angular.module('PORTAL.apierror')

  .config(['$stateProvider','LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider
    .state('apierror', {
      url: LOCATION + "/apierror",
      templateUrl: "app/apierror/index.html",
      controller: "ApiError"
    })

  }

})()