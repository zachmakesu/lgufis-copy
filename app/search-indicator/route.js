(function(){
  'use strict';

  angular.module('PORTAL.searchIndicator')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider

    .state("search-indicator", {
      url: "/search-indicator",
      templateUrl: "app/search-indicator/index.html",
      controller: "SearchIndicatorCtrl"
    })


  }

})()