(function(){
	'use strict';

  angular.module('PORTAL.searchIndicator')
  .controller('SearchIndicatorCtrl',['$scope','$stateParams','$state','FetchData','uiGmapGoogleMapApi','$filter',controllerFunc]) 

	function controllerFunc($scope,$stateParams,$state,FetchData,uiGmapGoogleMapApi,$filter){
		var vm = $scope;

    vm.mapMarker = function(model) {
      return '/images/map-marker-'+model['lgu_type'].toLowerCase()+'.png';
    }

    vm.currentI = function(id) {
    	console.log(id);
			vm.currentIndicator = id;
			console.log(vm.currentIndicator);
    }


	    FetchData.get('/lgu_names/all')
	    .then(
	      function(success){

	        vm.multipleLgus = success.data.data;

				vm.lguSelect = function(){
					var selectedLgus = [];
					console.log(selectedLgus);
					// console.log(vm.lgu);
				    
				    for (var i = 0; i < vm.lgus.length; i++) {
				       var lgu = vm.lgus[i];
				       // console.log(lgu);
				       if (lgu.included) {
				       		// console.log("test");
				       		selectedLgus.push(lgu.id)
				       }
				       
				    }

				    vm.selectedLgus = selectedLgus;

				    // var compareScores = [];

					FetchData.compare(selectedLgus)
					.then(
						function(success){

						  vm.selected = success.data.data;
						  console.log(vm.selected);

						  var lgusData = [];
						  console.log(lgusData);

						  	var lguSummary = {
						  		"datas": [],
						  		"name": [],
						  		// "series": []
						  	}

						  	console.log("summary",lguSummary);

						  for (var i = 0; i < vm.selected.length; i++) {
						  	console.log(vm.selected[i].name);
						  	var lguData = {
	  				        	"id": "",
		        				"name": "",
		        				"data": [],
		        				"labels": [],
		        				"latestData": ""

						  	}


						  	lguData.name = vm.selected[i].name;
						  	lguSummary.name.push(vm.selected[i].name);
						  	console.log(lguData);
						  	var compareLgusDatum = $filter('filter')(vm.selected[i].data_containers, {indicator_id:parseInt(vm.currentIndicator)},true)[0];
						  	console.log("filtered", compareLgusDatum);
						  	vm.currentIndName = compareLgusDatum.indicator_name;
						  	var data = [];
						  	var year = [];
						  		for (var key in compareLgusDatum.raw_data) {
						  				if(typeof compareLgusDatum.raw_data[key] == 'object') {
						  					var dataCheck = compareLgusDatum.raw_data[key].datum;
						  					var dataClean = dataCheck.replace(/\,|\%/g,"");
						  						if (dataCheck.match(/[a-z]/i)) {
						  							dataClean = 0;
						  						}

									  			data.push(dataClean);
									  			lguData.labels.push(compareLgusDatum.raw_data[key].year);
									  			year.push(compareLgusDatum.raw_data[key].year);
									  			// compareData.labels.push(compareLgusDatum[i].raw_data[key].year);


							  			}	
						  		}

						  	var latestData = data.pop();
						  	lguData.latestData = latestData;
						  	lguSummary.datas.push(latestData);

					  		lguData.data.push(data);
					  		lgusData.push(lguData);

					  		vm.lgusData = lgusData;
					  		vm.lguSummary = lguSummary;


						  } //for (var i = 0; i < vm.selected.length; i++)

						},
						function(error){
						  console.log('error get:',error);
						}
					); //FetchData.get(selectedLgus) 

				}

	      },
	      function(error){
	        console.log('error',error);
	      }
	    ); 


		FetchData.get('/indicators/all')
		.then(
			function(success){

			  vm.indicatorAll = success.data.data;

			},
			function(error){
			  console.log('error get:',error);
			}
		); 


	    // Limit Checkbox to 6
		vm.limit = 6;
    	vm.checked = 0;
	    vm.lguInclude = function(lgu){
	        if(lgu.included) vm.checked++;
	        else vm.checked--;console.log("Limit Reached");

	    }

		vm.clearAll = function() {    
		        angular.forEach(vm.lgus, function(lgu) {              
			        lgu.included = false;
			        vm.checked = 0;
			        vm.selectedIndicator = null;
			    });  
		};

		vm.staticData = [
			{
				id: 1,
				name: "ABORLAN",
				score: [
					{
						year: "2012",
						datum: "13.8%",
					},
					{
						year: "2013",
						datum: "10.8%",
					},
					{
						year: "2014",
						datum: "15.8%",
					}
				],
				rate: [
					{
						year: "2012",
						rate: "Good",
					},
					{
						year: "2013",
						rate: "Poor",
					},
					{
						year: "2014",
						rate: "Very Good",
					}
				]
			},
			{
				id: 2,
				name: "ABRA",
				score: [
					{
						year: "2012",
						datum: "9.8%",
					},
					{
						year: "2013",
						datum: "2.8%",
					},
					{
						year: "2014",
						datum: "5.8%",
					}
				],
				rate: [
					{
						year: "2012",
						rate: "Good",
					},
					{
						year: "2013",
						rate: "Poor",
					},
					{
						year: "2014",
						rate: "Very Good",
					}
				]
			},
			{
				id: 3,
				name: "ABRA DE ILOG",
				score: [
					{
						year: "2012",
						datum: "20.8%",
					},
					{
						year: "2013",
						datum: "15.8%",
					},
					{
						year: "2014",
						datum: "3.8%",
					}
				],
				rate: [
					{
						year: "2012",
						rate: "Good",
					},
					{
						year: "2013",
						rate: "Poor",
					},
					{
						year: "2014",
						rate: "Very Good",
					}
				]
			}
		];

		var lguChartOptions = {
		    "options": {
		    	"showScale": false,
		    	"scales": {
		    		"barThickness": 1,
				      yAxes: [
				        {
				          	display: false
				        }
				      ],
				      xAxes: [
				        {
				        	display: true
				        }
				      ]
		    	},
		        "legend": {
		            display: false,
		            labels: {
		                fontColor: '#fff'
		            }
		        }
		    }
		}

		$scope.chartOptions = lguChartOptions;


		var lguFeaturedData = [
			{	
		        "id": 1,
		        "name": "ABORLAN",
		        "data": [["7", "18", "9"]],
		        "labels": ["2011","2012","2013"],
		        "colours": [{ // default
		          "backgroundColor": ["#e74c3c","#2ecc71","#3498db"]
		        }]

	      	},
			{
		        "id": 2,
		        "name": "ABRA",
		        "data": [["12", "7", "19"]],
		        "labels": ["2011","2012","2013"],
		        "colours": [{ // default
		          "backgroundColor": ["#3498db","#e74c3c","#2ecc71"]
		        }]

	      	},
			{
		        "id": 3,
		        "name": "BAGUIO",
		        "data": [["4", "10", "17"]],
		        "labels": ["2011","2012","2013"],
		        "colours": [{ // default
		          "backgroundColor": ["#e74c3c","#3498db","#2ecc71"]
		        }]

	      	},
		];

		vm.lguCharts = lguFeaturedData;

		for (var key in lguFeaturedData) {

			var lguSingleData = {
				name : [],
				datas : [],
				colors : []
			}

			// console.log(lguFeaturedData[key].name);
			// console.log(lguFeaturedData[key].colours[0].backgroundColor[0]);
			// console.log(lguSingleData);
			// console.log(singleColor);
			// Push staticData.score.datum to "var datum = [];"
			for (var i = 0; i < lguFeaturedData.length; i++) {
				lguSingleData.name.push( lguFeaturedData[i].name );
				lguSingleData.datas.push( lguFeaturedData[i].data[0][0] );
				lguSingleData.colors.push( lguFeaturedData[i].colours[0].backgroundColor[0] );
			}


		}

		vm.summaryChart = lguSingleData;
		console.log(vm.summaryChart);

		$scope.summaryOptions = {
			responsive: true,
		}


		for  (var i = 0; i < vm.staticData.length; i++) {
			vm.barData = vm.staticData[i].score;
		}


	    // vm.lguSelect = function(lguName) {
	    //     var i = $.inArray(lguName, vm.selectedLgus);
	    //     if (i > -1) {
	    //         vm.selectedLgus.splice(i, 1);
	    //     } else {
	    //         vm.selectedLgus.push(lguName);
	    //     }
	    // }

	    vm.lguFilter = function(lguChecked) {
	        if (vm.lguCheck.length > 0) {
	            if ($.inArray(lguChecked.selected) < 0)
	                return;
	        }
	        
	        return lguChecked;
	    }


		$scope.lguFilter = [];


		$scope.map  = {
		  "center": {
		    "latitude": 11.6736111,
		    "longitude": 118.1259741
		  },
		  zoom: 6,
		}

		$scope.options = {
			"scrollwheel": false,
		}
		
        $scope.windowOptions = {
            visible: false
        };

        $scope.onClick = function() {
            $scope.windowOptions.visible = !$scope.windowOptions.visible;
        };

        $scope.closeClick = function() {
            $scope.windowOptions.visible = false;
        };

      angular.element(document).ready(function () {
          $(".top-bar").addClass("animated visible slideInDown");
          setTimeout('$(".angular-google-map").addClass("animated visible slideInDown")', 800);
          setTimeout('$(".search-section").addClass("animated visible slideInLeft")', 1400);
          setTimeout('$(".search-section").removeClass("animated visible slideInLeft hidden")', 2400);
          setTimeout("$('#show-hide-2 .fa-angle-left').addClass('animated infinite slideInRight')", 3400)
      });

      $("#show-hide-2").click(function() {
        $('.search-section').toggleClass('search-close');
        $('#show-hide-2 .fa-angle-right').toggleClass('slideInLeft');
        $('#show-hide-2 .fa-angle-left').toggleClass('slideInRight'); 
      });


		// console.log($scope.schedules);

		// vm.compareResult = [];
		// $(document).on('click','.lgu-list-box',function(){
		// 	var input = $(this).find('input');
		// 	var box = $(this).find('.lguCheck');
		// 	var lguName = input.attr('id');


		// 	if (input.prop('checked')) {
		// 		input.removeAttr('checked');
		// 		box.addClass('lguChecked');
		// 		vm.compareResult.push(lguName);
		// 	} else {
		// 		input.prop('checked',true);
		// 		box.removeClass('lguChecked');
		// 		var index = vm.compareResult.indexOf(lguName)
		// 		vm.compareResult.splice(index,1);
		// 	}

		// 	console.log(vm.compareResult);
		// })

	}
})()