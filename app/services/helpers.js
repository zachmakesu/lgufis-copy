(function(){
	'use strict';

	angular.module('PORTAL.services')

	.service('Helpers', [serviceFunc])

	function serviceFunc() {
			var self = this;

			this.pageTitle = function() {
				// console.log('test');

				return $("#title").text();
			};

			this.getSlug = function(slug) {
				var slug = slug ? slug : self.pageTitle().split(' ').join('-').toLowerCase();
				return slug;
			};

			this.toSlug = function(title) {
				var slug = title.split(' ').join('-').toLowerCase();
				return slug;
			}

			this.getPath = function(part) {
				if (part) {

					var pathArr = window.location.pathname.split('/');
					pathArr.shift();
					var result = pathArr[part];
				} else {
					var result = window.location.pathname;
				}

				return result;
			};

			this.getKeys = function(obj){
				var keys = [];
				for(var key in obj[0]){
				  keys.push(key);
				}
				return keys;
			}



	}

})()