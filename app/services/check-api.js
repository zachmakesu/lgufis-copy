(function(){
	'use strict';

	angular.module('PORTAL.services')

	.factory('CheckApi', ['$http','$state','API_ENDPOINT',serviceFunc])

	function serviceFunc($http,$state,API_ENDPOINT) {
		return {
			status: function() {
		  	$http.get(API_ENDPOINT).error(function(data, status, headers, config){
		  		$state.go('apierror');
		  	})
			},
			isRunning: function() {
		  	$http.get(API_ENDPOINT)
		  	.success(function(data, status, headers, config){
		  		$state.go('dashboard');
		  	})
		  	.error(function(data, status, headers, config){
		  		$state.go('apierror');
		  	})
			}
		}
	}

})()