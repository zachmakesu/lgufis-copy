(function(){
	'use strict';

	angular.module('PORTAL.services')

	.factory('Page', [serviceFunc])

	function serviceFunc() {
		var title = "default"
		return {
			title: function() {
		  	return title;
			},
			setTitle: {
				function (newTitle) {
					title = newTitle;
				}
			}
		}
	}

})()