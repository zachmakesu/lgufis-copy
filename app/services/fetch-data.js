(function(){
	'use strict';

	angular.module('PORTAL.services')

	.factory('FetchData', ['$http','API_ENDPOINT','Helpers',serviceFunc])

	function serviceFunc($http,API_ENDPOINT,Helpers) {
		var date = new Date();
		var today = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate().toString().length == '1' ? '0'+date.getDate() : date.getDate());

		var creds = {
			n : 'LGU',
			s : '5d3d57eb3875c01aec9b053bec1d503ba7418948ea8b9a43b6d3107469a3104fe0a650b65619dbb30d0207c8ca96c0f438313b4d299c25cf1dbdbed1dc4cce0c_' + today
		}

		var auth = function(url,params) {
			var hmac = CryptoJS.HmacSHA256('/api/v1/front'+url+params,creds.s).toString();
			return hmac;
		}

		return {
			get: function(url,params) {
				params = params ? params : '';

				return $http({
					  method: 'GET',
					  url: API_ENDPOINT + url,
					  headers : {
					  	'Authorization': creds.n + ' ' + auth(url,params)
					  }
					})
			},

			compare: function(ids) {
				if (typeof ids == 'object') {
					// ids = ids.join().replace(',','^')
					ids = ids.join('^', ids)
				}
				var params = 'lgu_name_ids=' + ids
				var url = '/lgu_names/compare?lgu_name_ids=' + ids;


				return $http({
					  method: 'GET',
					  url: API_ENDPOINT + url,
					  headers : {
					  	'Authorization': creds.n + ' ' + CryptoJS.HmacSHA256('/api/v1/front/lgu_names/compare|lgu_name_ids='+ids,creds.s).toString()
					  }
					})
			},
			search: function(url,str) {
				var params = '|search='+str;
				return $http({
					  method: 'GET',
					  url: API_ENDPOINT + url + '?search='+str,
					  headers : {
					  	'Authorization': creds.n + ' ' + auth(url,params)
					  }
					})
			},
			geoloc: function() {
				return $http({
					  method: 'GET',
					  url: '/data/geo.json'
					})
			}
		}
	}

})()