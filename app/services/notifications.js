(function(){
	'use strict';

	angular.module('PORTAL.services')

	.factory('Notification', ['$http','API_ENDPOINT_URL',serviceFunc])

	function serviceFunc($http,API_ENDPOINT_URL) {
		return {
			fetchAll: function() {
				return $http.get(API_ENDPOINT_URL + "/notifications");
			},
			fetchNew: function() {
				return $http.get(API_ENDPOINT_URL + "/notifications/new");
			},
			countNew: function() {
				return $http.get(API_ENDPOINT_URL + "/notifications/count");
			}
		}
	}

})()