(function(){
	'use strict';

	angular.module('PORTAL.services')

	.factory('Activity', ['$http','API_ENDPOINT_URL',serviceFunc])

	function serviceFunc($http,API_ENDPOINT_URL) {
		return {
			fetch: function() {
				return $http.get(API_ENDPOINT_URL + "/activity")
			}
		}
	}

})()