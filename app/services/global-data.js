(function(){
	'use strict';

	angular.module('PORTAL.services')

	.service('GlobalData', [serviceFunc])

	function serviceFunc($http,API_ENDPOINT_URL) {
		var self = this;


				this.lgus = [
					{
						"id": 0,
						"slug": "naga-city",
						"lguName": "Naga City",
						"quickFacts" : "Naga, officially called City of Naga, or simply Naga City, is an independent component city in the Bicol Region of the Philippines.",
						"markerIcon": "images/map-marker-city.png",
						"location": {
							"latitude": "13.6408034",
							"longitude": "123.2004238"
						},
						"lguType": "City",
						"incomeClass": "1",
						"rating": {
							"2014": 5,
							"2013": 4,
							"2012": 3,
							"2011": 2,
						},
						"indicatorCategory": {
							"revenue": {
								"regularIncomeLevel": {
									"name": "Regular Income Level",
									"rate": "NI",
									"score": 4
								},
								"localRevenueLevel": {
									"name": "Local Revenue Level",
									"rate": "NI",
									"score": 6
								},
								"localRevenueGrowth": {
									"name": "Local Revenue Growth",
									"rate": "11.3%",
									"score": 15
								}
							},
							"expenditures": {
								"totalExpenditure": {
									"name": "Total Expenditures",
									"rate": "Low",
									"score": 2
								},
								"ldpUtilization": {
									"name": "20% LDP Utilization Rating",
									"rate": "NI",
									"score": 6
								},
								"localRevenueGrowth": {
									"name": "Local Revenue Growth",
									"rate": "Failed",
									"score": "17.3%"
								}
							}
						}
					},
					{
						"id": 1,
						"slug": "catanduanes",
						"lguName": "Catanduanes",
						"quickFacts": "Catanduanes, officially called Province of Catanduanes, or simply Catanduanes City, is an independent component city in the Bicol Region of the Philippines.",
						"markerIcon": "images/map-marker-province.png",
						"location": {
							"latitude": "13.587665",
							"longitude": "124.198822"
						},
						"lguType": "Province",
						"incomeClass": "4",
						"rating": {
							"2014": 5,
							"2013": 4,
							"2012": 3,
							"2011": 2,
						}
					},
					{
						"id": 2,
						"slug": "tabaco-city",
						"lguName": "Tabaco City",
						"quickFacts": "Tabaco, officially called City of Tabaco, or simply Tabaco City, is an independent component city in the Bicol Region of the Philippines.",
						"markerIcon": "images/map-marker.png",
						"location": {
							"latitude": "13.3301242",
							"longitude": "123.6532371"
						},
						"lguType": "City",
						"incomeClass": "2",
						"rating": {
							"2014": 5,
							"2013": 4,
							"2012": 3,
							"2011": 2,
						}
					},
					{
						"id": 3,
						"slug": "legazpi-city",
						"lguName": "Legazpi City",
						"quickFacts": "Legazpi, officially called City of Legazpi, or simply Legazpi City, is an independent component city in the Bicol Region of the Philippines.",
						"markerIcon": "images/map-marker2.png",
						"location": {
							"latitude": "13.1209017",
							"longitude": "123.7031342"
						},
						"lguType": "City",
						"incomeClass": "2",
						"rating": {
							"2014": 5,
							"2013": 4,
							"2012": 3,
							"2011": 2,
						}
					},
					{
						"id": 4,
						"slug": "sorsogon-city",
						"lguName": "Sorsogon City",
						"quickFacts": "Sorsogon, officially called City of Sorsogon, or simply Sorsogon City, is an independent component city in the Bicol Region of the Philippines.",
						"markerIcon": "images/map-marker.png",
						"location": {
							"latitude": "13.0240931",
							"longitude": "123.9450964"
						},
						"lguType": "City",
						"incomeClass": "3",
						"rating": {
							"2014": 5,
							"2013": 4,
							"2012": 3,
							"2011": 2,
						}
					},
					{
						"id": 5,
						"slug": "pandan-catanduanes",
						"lguName": "Pandan, Catanduanes",
						"quickFacts": "Pandan, officially called Municipality of Pandan, or simply Pandan, is an independent component city in the Bicol Region of the Philippines.",
						"markerIcon": "images/map-marker.png",
						"location": {
							"latitude": "14.0348556",
							"longitude": "124.159176"
						},
						"lguType": "Municipality",
						"incomeClass": "3",
						"rating": {
							"2014": 5,
							"2013": 4,
							"2012": 3,
							"2011": 2,
						}
					},
					{
						"id": 6,
						"slug": "bato-catanduanes",
						"lguName": "Bato, Catanduanes",
						"quickFacts": "Bato, officially called Municipality of Bato, or simply Bato, is an independent component city in the Bicol Region of the Philippines.",
						"markerIcon": "images/map-marker.png",
						"location": {
							"latitude": "13.588615",
							"longitude": "124.2732089"
						},
						"lguType": "Municipality",
						"incomeClass": "3",
						"rating": {
							"2014": 5,
							"2013": 4,
							"2012": 3,
							"2011": 2,
						}
					}

				];

				this.schedule = [
					{
						scheduleName: "Fashionology Talks",
						scheduleTime: '10:00 AM - 12:00 NN',
						scheduleDetails: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
					},
					{
						scheduleName: "Designers' Showroom",
						scheduleTime: '3:00 PM',
						scheduleDetails: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
					},
					{
						scheduleName: "International Designer Presentation",
						scheduleTime: '4:00 PM',
						scheduleDetails: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
					},
					{
						scheduleName: "Buyers' Show",
						scheduleTime: '6:30 PM',
						scheduleDetails: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
					},
				];


	}

})()