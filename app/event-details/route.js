(function(){
  'use strict';

  angular.module('PORTAL.eventDetails')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider

    .state("event-details", {
      url: "/event-details",
      templateUrl: "app/event-details/index.html",
      controller: "EventDetailsCtrl"
    })


  }

})()