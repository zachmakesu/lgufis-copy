(function(){
  'use strict';

  angular.module('PORTAL.page2sample')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider

    .state("page2sample", {
      url: "/page2sample",
      templateUrl: "app/page2sample/index.html",
      controller: "Page2SampleCtrl"
    })


  }

})()