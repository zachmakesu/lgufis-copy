(function(){
	'use strict';

  angular.module('PORTAL.page2sample')
  .controller('Page2SampleCtrl',['$scope','GlobalData',controllerFunc])


	function controllerFunc($scope,GlobalData){
		var vm = $scope;

		console.log({data1: GlobalData.data1});
		console.log({data2: GlobalData.data2});

	}

})()