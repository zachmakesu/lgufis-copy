(function(){
	'use strict';

	angular.module('PORTAL.widgets')

	.directive('newsWidget', 	function() {
		return {
			restrict: 'A',
			templateUrl: 'app/widgets/news/index.html',
			controller: 'NewsWidgetCtrl'
		}
	});

})()