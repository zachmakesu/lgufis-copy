(function(){
	'use strict';

	angular.module('PORTAL.widgets')

	.controller('NewsWidgetCtrl',['$scope','FetchData',controllerFunc]);

	function controllerFunc ($scope, FetchData) {

		FetchData.fetch('news').then(function(response){
			$scope.newsFeeds = response.data;
		})

	}

})()