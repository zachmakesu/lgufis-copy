(function(){
	'use strict';

	angular.module('PORTAL.widgets')

	.controller('ActivityWidget',['$scope','Activity',controllerFunc]);

	function controllerFunc ($scope, Activity) {

		Activity.fetch().then(function(response){
			$scope.newActivity = response.data;
		})

	}

})()