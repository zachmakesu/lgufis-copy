(function(){
	'use strict';

	angular.module('PORTAL.widgets')

	.directive('activityWidget', 	function() {
		return {
			restrict: 'A',
			templateUrl: 'app/widgets/activity/index.html',
			controller: 'ActivityWidget'
		}
	});

})()