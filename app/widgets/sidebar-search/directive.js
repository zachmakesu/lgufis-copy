(function(){
	'use strict';

	angular.module('PORTAL.widgets')

	.directive('sidebarSearch', function() {
		return {
			restrict: 'A',
			templateUrl: 'app/components/sidebar-search/index.html'
			// controller: 'SidebarSearch'
		}
	});

})()