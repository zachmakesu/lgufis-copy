(function(){
	'use strict';

	angular.module('PORTAL.widgets')

	.controller('NotificationWidget',['$scope','Notification',controllerFunc]);

	function controllerFunc ($scope, Notification) {

		Notification.fetchNew().then(function(response){
			$scope.newNotifications = response.data;
		})

		Notification.countNew()
		.then(function(result){
			$scope.count = result.data.count;
		})
	}

})()