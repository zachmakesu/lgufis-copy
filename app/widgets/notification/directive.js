(function(){
	'use strict';

	angular.module('PORTAL.widgets')

	.directive('notificationWidget', 	function() {
		return {
			restrict: 'A',
			templateUrl: 'app/widgets/notification/index.html',
			controller: 'NotificationWidget'
		}
	});

})()