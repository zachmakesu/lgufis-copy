(function(){
	'use strict';

  angular.module('PORTAL.widgets')
  .controller('SearchLgu',['$scope','FetchData',controllerFunc])

	function controllerFunc($scope,FetchData){
        if (!String.prototype.trim) {
          (function() {
            // Make sure we trim BOM and NBSP
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function() {
              return this.replace(rtrim, '');
            };

          })();
        }

        [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
          // in case the input is already filled..
          if( inputEl.value.trim() !== '' ) {
            classie.add( inputEl.parentNode, 'input--filled' );
          }

          // events:
          inputEl.addEventListener( 'focus', onInputFocus );
          inputEl.addEventListener( 'blur', onInputBlur );

        } );

        function onInputFocus( ev ) {
          classie.add( ev.target.parentNode, 'input--filled' );
          $(".search-results").addClass("show-results");
        }

        function onInputBlur( ev ) {
          if( ev.target.value.trim() === '' ) {
            classie.remove( ev.target.parentNode, 'input--filled' );
            $(".search-results").removeClass("show-results");
          }
        }

        var typingTimer;
        var doneTypingInterval = 100;
        var $input = $('#lguSearchFilter');

        $input.on('keyup', function () {
          clearTimeout(typingTimer);
          typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

        $input.on('keydown', function () {
          clearTimeout(typingTimer);
        });

        //user is "finished typing," do something
        function doneTyping () {

          $scope.$apply(function (){

            FetchData.search('/lgu_names',$('#lguSearchFilter').val())
              .then(
                function(success){
                  $scope.lgus = success.data.data;
                  for (var i = 0; i < $scope.lgus.length; i++) {
                  }
                },
                function(error){
                  console.log('error',error);
                }
              );
          });
        }


	}

})()