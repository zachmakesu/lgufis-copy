(function(){
	'use strict';

	angular.module('PORTAL.widgets')

	.directive('searchLgu', function() {
		return {
			restrict: 'A',
			templateUrl: 'app/widgets/search-lgu/index.html',
			controller: 'SearchLgu'
		}
	});

})()