(function(){
  'use strict';

  angular.module('PORTAL.lguExpenditures')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider
    
    .state("lgu-expenditures", {
      url: "/search-lgu/:id/expenditures",
      templateUrl: "app/lgu-expenditures/index.html",
      controller: "LguExpenditures"
    });


  }

})()