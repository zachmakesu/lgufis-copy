(function(){
	'use strict';

  angular.module('PORTAL.lguExpenditures')

  .controller('LguExpenditures',['$scope','$state','$stateParams','FetchData','$filter',controllerFunc])

	function controllerFunc($scope,$state,$stateParams,FetchData,$filter){

		var vm = $scope;

		var current = parseInt($stateParams.id);
		vm.current = current;

		vm.currentIndicator = 1;

		vm.currentI = function(currId) {
			// console.log("current", currId);
			vm.currentIndicator = currId;
			console.log(vm.currentIndicator);
		}


		FetchData.get('/lgu_names/'+$stateParams.id,'|id='+$stateParams.id)
		.then(
			function(success){
			  // console.log('success',success.data.data);
			  vm.currentLgu = success.data.data;
			  console.log("log", vm.currentLgu);

			},
			function(error){
			  console.log('error get:',error);
			}
		);

		FetchData.get('/indicators/all')
		.then(
			function(success){
			  vm.indicatorAll = success.data.data;

			},
			function(error){
			  console.log('error get:',error);
			}
		);

		FetchData.get('/lgu_names/all')
		.then(
			function(success){
			  vm.lguList = success.data.data;
			  console.log("lguList", vm.lguList);

				vm.lguSelect = function(){
					var selectedLgus = [current];
				    
				    for (var i = 0; i < vm.lguList.length; i++) {
				       var lgu = vm.lguList[i];
				       if ( lgu.checked) {
				       	console.log("checked",lgu.checked);
				        selectedLgus.push(lgu.id)  
				       }
				       
				    }



				    vm.selectedLgus = selectedLgus;

				    var compareScores = [];

					FetchData.compare(selectedLgus)
					.then(
						function(success){
						  vm.lguIndicator = success.data.data;
						  // var compareLgusDatum = vm.lguIndicator.data_containers;
						  	// console.log(vm.lguIndicator);

					  		var compareData = {
					  			"data": [],
					  			"series": [],
					  			"labels": [],
								"options": {
								    "tooltips": {
								        callbacks: {
								            label: function(tooltipItem, data) {
								                return Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
								                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
								                });
								            }
								        }
								    },
							    	"scales": {
									      yAxes: [
									        {
									          type: 'linear',
									          display: false,
									          position: 'left',
									                ticks: {
									                    beginAtZero:true,
									                    fontColor: '#fff'
									                }
									        }
									      ],
									      xAxes: [
									        {
								                ticks: {
								                    fontColor: '#fff'
								                }
									        }
									      ]
							    	},
							        "legend": {
							            display: true,
							            labels: {
							                fontColor: '#fff'
							            }
							        }
							    }

					  		}
						  for (var i = 0; i < vm.lguIndicator.length; i++) {

					  		compareData.series.push(vm.lguIndicator[i].name);

						  	var compareLgusDatum = $filter('filter')(vm.lguIndicator[i].data_containers, {indicator_id:parseInt(vm.currentIndicator)},true)[0];
						  	var data = [];
						  	var year = [];
					  		console.log("raw",compareLgusDatum);
						  	console.log(data);
					  		for (var key in compareLgusDatum.raw_data) {
					  				if(typeof compareLgusDatum.raw_data[key] == 'object') {
					  					var dataCheck = compareLgusDatum.raw_data[key].datum;
					  					var dataClean = dataCheck.replace(/\,|\%/g,"");
					  						if (dataCheck.match(/[a-z]/i)) {
					  							// dataClean = 0;

						            			switch (dataCheck.toLowerCase()) {
						            				case "ni" :
						            					console.log("NI");
						            					data.push(0);
						            					break;
						            				case "poor" :
						            					console.log("Poor");
						            					data.push(2);
						            					break;
						            				case "fair" :
						            					console.log("Fair");
						            					data.push(3);
						            					break;
						            				case "good" :
						            					console.log("Good");
						            					data.push(4);
						            					break;
						            				case "very good" :
						            					console.log("Very Good");
						            					data.push(5);
						            					break;
						            				case "none" :
						            					console.log("None");
						            					data.push(0);
						            					break;
						            				case "very low" :
						            					console.log("Very Low");
						            					data.push(1);
						            					break;
						            				case "low" :
						            					console.log("Low");
						            					data.push(2);
						            					break;
						            				case "fair" :
						            					console.log("Fair");
						            					data.push(3);
						            					break;
						            				case "high" :
						            					console.log("High");
						            					data.push(4);
						            					break;
						            				case "very high" :
						            					console.log("Very High");
						            					data.push(5);
						            					break;
						            				case "non-compliant" :
						            					console.log("Non-Compliant");
						            					data.push(0);
						            					break;
						            				case "compliant" :
						            					console.log("Compliant");
						            					data.push(1);
						            					break;


						            			}
					  						} else {
								  				data.push(dataClean);
					  						}

								  			year.push(compareLgusDatum.raw_data[key].year);
								  			// compareData.labels.push(compareLgusDatum[i].raw_data[key].year);


						  			}	
					  		}

					  		compareData.data.push(data);
					  		compareData.labels.push(year);

						 	vm.compareLgus = compareData;		
	
							console.log(vm.compareLgus);

					  			

						  } //for (var i = 0; i < vm.lguIndicator.length; i++) {

						},
						function(error){
						  console.log('error get:',error);
						}
					);
				    
				    // vm.compareDatums = compareScores;
				    // console.log("compareDatums", vm.compareDatums);

    				// vm.selectedLgus = selectedLgus;
				} // vm.lguSelect = function(){

			}, 
			function(error){
			  console.log('error get:',error);
			}
		);
	    

	var scoreCharts = [];

    FetchData.get('/lgu_names/'+current+'/indicators','|id='+current)
      .then(
        function(success){
            vm.chartData = success.data.data;
            vm.overAll = vm.chartData.overall_ratings;
            var availableData = vm.chartData.data_containers;

            // Initial Data
            var initialIndicator = $filter('filter')(availableData, {indicator_id:54},true)[0];

            	var initialChart = {
            		"id": "",
            		"name": "",
            		"data_type": "",
            		"latestData": "",
            		"trend": "",
            		"data": [],
            		"label": []
            	}
            	
            	vm.score = initialChart;


            	initialChart.name = initialIndicator.indicator_name;
            	initialChart.id = initialIndicator.indicator_id;

		            var datum = [];
		            var getLatest = [];
		            console.log("datum", datum)
		            if(typeof initialIndicator.raw_data == 'object') {
			            for (var i = 0; i < initialIndicator.raw_data.length; i++) {
			            		var dataCheck = initialIndicator.raw_data[i].datum;
			            		var dataClean = dataCheck.replace(/\,|\%/g,"");

			            		if (dataCheck.match(/[a-z]/i) ) {
			            			initialChart.data_type = "text";
			            		} else if (dataCheck.match(/%/i) ) {
			            			initialChart.data_type = "percent";
			            		} else if (dataCheck.length > 6 ) {
			            			initialChart.data_type = "million";
			            		} else {
			            			initialChart.data_type = "number";
			            		}

			            		datum.push(dataClean);
			            		getLatest.push(dataClean);
			            		initialChart.label.push(initialIndicator.raw_data[i].year);
			            }
		          	}
            	
            		initialChart.data.push(datum);
            		initialChart.latestData = getLatest.pop();
            		if (datum[datum.length - 1] > datum[datum.length - 2]) {
            			initialChart.trend= "up";
            			console.log("up");
            		} else if (datum[datum.length - 1] < datum[datum.length - 2]) {
            			initialChart.trend= "down";
            			console.log("down");
            		} else {
            			initialChart.trend= "equal";
            			console.log("equal");
            		}
	            	var trend = datum[datum.length - 2];
	            	console.log("trend",trend);


            // On Indicator Change

            vm.indicatorChange = function() {
	            var indicatorSelected = $filter('filter')(availableData, {indicator_id:parseInt(vm.currentIndicator)},true)[0];
	            console.log("selected only", indicatorSelected);
            	var scoreChart = {
            		"id": "",
            		"name": "",
            		"data_type": "",
            		"trend": "",
            		"latestData": "",
            		"data": [],
            		"text_data": [],
            		"label": [],
            		"text_length": ""
            	}

            	console.log("score",scoreChart);
            	vm.score = scoreChart;

            	scoreChart.name = indicatorSelected.indicator_name;
            	if (scoreChart.name.length > 50) {
            		scoreChart.text_length = "long-text";
            	} else {
            		scoreChart.text_length = "short-text";
            	}
            	scoreChart.id = indicatorSelected.indicator_id;

            	console.log("raw_data", indicatorSelected.raw_data);

		            var datum = [];
		            var data2 = [];
		            var getLatest = [];
		            console.log("datum", datum)
		            if(typeof indicatorSelected.raw_data == 'object') {
			            for (var i = 0; i < indicatorSelected.raw_data.length; i++) {
			            		var dataCheck = indicatorSelected.raw_data[i].datum;
			            		var dataClean = dataCheck.replace(/\,|\%/g,"");

			            		if (dataCheck.match(/[a-z]/i) ) {
			            			scoreChart.data_type = "text";
			            			switch (dataCheck.toLowerCase()) {
			            				case "ni" :
			            					console.log("NI");
			            					data2.push(0);
			            					break;
			            				case "poor" :
			            					console.log("Poor");
			            					data2.push(2);
			            					break;
			            				case "fair" :
			            					console.log("Fair");
			            					data2.push(3);
			            					break;
			            				case "good" :
			            					console.log("Good");
			            					data2.push(4);
			            					break;
			            				case "very good" :
			            					console.log("Very Good");
			            					data2.push(5);
			            					break;
			            				case "none" :
			            					console.log("None");
			            					data2.push(0);
			            					break;
			            				case "very low" :
			            					console.log("Very Low");
			            					data2.push(1);
			            					break;
			            				case "low" :
			            					console.log("Low");
			            					data2.push(2);
			            					break;
			            				case "fair" :
			            					console.log("Fair");
			            					data2.push(3);
			            					break;
			            				case "high" :
			            					console.log("High");
			            					data2.push(4);
			            					break;
			            				case "very high" :
			            					console.log("Very High");
			            					data2.push(5);
			            					break;
			            				case "non-compliant" :
			            					console.log("Non-Compliant");
			            					data2.push(0);
			            					break;
			            				case "compliant" :
			            					console.log("Compliant");
			            					data2.push(1);
			            					break;

			            			}

			            		} else if (dataCheck.match(/%/i) ) {
			            			scoreChart.data_type = "percent";
			            		} else if (dataCheck.length > 6 ) {
			            			scoreChart.data_type = "million";
			            		} else {
			            			scoreChart.data_type = "number";
			            		}

			            		datum.push(dataClean);
			            		getLatest.push(dataClean);
			            		scoreChart.label.push(indicatorSelected.raw_data[i].year);
			            }
		          	}
            	
            		scoreChart.data.push(datum);
            		scoreChart.text_data.push(data2);
            		scoreChart.latestData = getLatest.pop();
            		scoreCharts.push(scoreChart);

            		if (datum[datum.length - 1] > datum[datum.length - 2]) {
            			scoreChart.trend= "up";
            			console.log("up");
            		} else if (datum[datum.length - 1] < datum[datum.length - 2]) {
            			scoreChart.trend= "down";
            			console.log("down");
            		} else {
            			scoreChart.trend= "equal";
            			console.log("equal");
            		}

            } //vm.indicatorChange = function() {



          	// For simulateOption

          		var simulation = {
          			"data": [],
          			"labels": [],
          			"series":[]
          		}

          		vm.loadInit = function(id) {

	          		var currentOnly = $filter('filter')(availableData, {indicator_id:parseInt(id)},true)[0];
	          		console.log("indicator",currentOnly);
	          		vm.simulateDetails = currentOnly;

	          		var datum = [];
	          		var year = [];

		            if(typeof currentOnly.raw_data == 'object') {
		          		for (var i = 0; i < currentOnly.raw_data.length; i++) {
		          			var dataCheck = currentOnly.raw_data[i].datum;
		          			var dataClean = dataCheck.replace(/\,|\%/g,"");
		  						if (dataCheck.match(/[a-z]/i)) {
			            			switch (dataCheck.toLowerCase()) {
			            				case "ni" :
			            					console.log("NI");
			            					datum.push(0);
			            					break;
			            				case "poor" :
			            					console.log("Poor");
			            					datum.push(2);
			            					break;
			            				case "fair" :
			            					console.log("Fair");
			            					datum.push(3);
			            					break;
			            				case "good" :
			            					console.log("Good");
			            					datum.push(4);
			            					break;
			            				case "very good" :
			            					console.log("Very Good");
			            					datum.push(5);
			            					break;
			            				case "none" :
			            					console.log("None");
			            					datum.push(0);
			            					break;
			            				case "very low" :
			            					console.log("Very Low");
			            					datum.push(1);
			            					break;
			            				case "low" :
			            					console.log("Low");
			            					datum.push(2);
			            					break;
			            				case "fair" :
			            					console.log("Fair");
			            					datum.push(3);
			            					break;
			            				case "high" :
			            					console.log("High");
			            					datum.push(4);
			            					break;
			            				case "very high" :
			            					console.log("Very High");
			            					datum.push(5);
			            					break;
			            				case "non-compliant" :
			            					console.log("Non-Compliant");
			            					datum.push(0);
			            					break;
			            				case "compliant" :
			            					console.log("Compliant");
			            					datum.push(1);
			            					break;


			            			}
		  						} else {
		          					datum.push(dataClean);
		  						}

		          			year.push(currentOnly.raw_data[i].year);
		          		}

		          	}
		          	
		          	var lastThreeData = datum.slice(Math.max(datum.length - 3));
	          		var lastThreeLabel = year.slice(Math.max(year.length - 3));
	          		simulation.data.push(lastThreeData);
	          		simulation.labels = lastThreeLabel;
	          		simulation.series.push(vm.chartData.name);
	      			console.log("testSingle Raw",simulation);

          		}


	  			var simData = [];
	  			var simLabel = "Simulation";

		  		vm.pushData = function(isValid,data) {
		  			simData[0] = data.data1;
		  			simData[1] = data.data2;
		  			simData[2] = data.data3;
		  			console.log(simulation.data.length);
		  			if (simulation.data.length < 2) {
						simulation.data.push(simData);
						simulation.series.push(simLabel);
		  			}
		  		}

				vm.clearChart = function() {
					// console.log(simulation.data.length);
					simulation.data = [];
					simulation.labels = [];
					simulation.series = [];
					console.log("clear info", simulation);
					vm.showSimulate = false;
				}
			  				
          		console.log("testSingle",vm.chartData);
          		// console.log("kasalu",vm.currentIndicator);
          		vm.simChart = simulation;

      		// END simulateOption

        },

        function(error){
          console.log('error:',error);
        }
      ); //end of FetchData.get

			vm.scoreCards = scoreCharts;
			console.log("score chart:", vm.scoreCards);
			// console.log("selectedIndicator:", vm.selectedIndicator);


		  vm.colors = {
			fillColor: "rgba(245, 157, 21,0.2)",
      		strokeColor: "rgba(245, 157, 21,1)",
      		pointColor: "rgba(245, 157, 21,1)"
		  }



		  vm.optionsLarge = {
		    "tooltips": {
		        callbacks: {
		            label: function(tooltipItem, data) {
		                return Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
		                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
		                });
		            }
		        }
		    },
		    scales: {
		      yAxes: [
		        {
		          type: 'linear',
		          display: false,
		          position: 'left',
                  gridLines: {
                    display: false,
                	}
		        }
		      ],
		      xAxes: [
		        {
                  gridLines: {
                    display: false,
                	}
		        }
		      ]

		    }
		  }


		vm.textOption = {
	        scales: {
	            yAxes: [{
                  	gridLines: {
	                    display: false,
                	},
                	display: false,
	                ticks: {
	                    min:0,
	                    max:5,
	                }
	            }],
	            xAxes: [{
	            	// display: false,
                  	gridLines: {
                		display: false,
            		}
	            }]
	        }
		}

		vm.compareTextOption = {
			"options": {
			    "tooltips": {
			        callbacks: {
			            label: function(tooltipItem, data) {
			                return Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
			                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
			                });
			            }
			        }
			    },
		    	"scales": {
				      yAxes: [
				        {
				          display: false,
				          position: 'left',
				                ticks: {
	                    			min:0,
	                    			max:5,
				                    fontColor: '#fff'
				                }
				        }
				      ],
				      xAxes: [
				        {
			                ticks: {
			                    fontColor: '#fff'
			                }
				        }
				      ]
		    	},
		        "legend": {
		            display: true,
		            labels: {
		                fontColor: '#fff'
		            }
		        }
		    }
		}

		vm.percentOption = {
	        scales: {
	            yAxes: [{
                  	gridLines: {
	                    display: false,
                	},
                	display: false,
	                ticks: {
	                    min:0,
	                    max:100,
	                    stepSize:5
	                }
	            }],
	            xAxes: [{
	            	// display: false,
                  	gridLines: {
                		display: false,
            		}
	            }]
	        }
		}

		  vm.simulateOption = {
				"options": {
				    "tooltips": {
				        callbacks: {
				            label: function(tooltipItem, data) {
				                return Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
				                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
				                });
				            }
				        }
				    },
			    	"scales": {
					      yAxes: [
					        {
					          type: 'linear',
					          display: false,
					          position: 'left',
					                ticks: {
					                    beginAtZero:true,
					                    fontColor: '#000'
					                }
					        }
					      ],
					      xAxes: [
					        {
				                ticks: {
				                    fontColor: '#000'
				                }
					        }
					      ]
			    	},
			        "legend": {
			            display: true,
			            labels: {
			                fontColor: '#000'
			            }
			        }
			    }
		  }


	// vm.compareData = lguCompareData;


	    vm.showPrompt = false;
		vm.limit = 3;
    	vm.checked = 0;
	    vm.checkChanged = function(lgu){
	        if(lgu.checked) vm.checked++;
	        else vm.checked--;console.log("Limit Reached");
	        if (vm.checked == 3) {
	        	vm.showPrompt = !vm.showPrompt;
	        } else {
	        	vm.showPrompt = false;
	        }
	    }

		vm.closePrompt = function() {
			vm.showPrompt = false;
		}

	    // SUMMARY MODAL
		vm.showSummary = false;
		vm.toggleSummary = function() {
			vm.showSummary = !vm.showSummary;
			console.log("clicked", vm.showSummary);
		}

		vm.closeSummary = function() {
			vm.showSummary = false;
		}

	    // FEEDBACK MODAL
		vm.showFeedback = false;
		vm.toggleFeedback = function() {
			vm.showFeedback = !vm.showFeedback;
			console.log("clicked", vm.showFeedback);
		}

		vm.closeFeedback = function() {
			vm.showFeedback = false;
		}

	    // SHARE MODAL
		vm.showShare = false;
		vm.toggleShare = function() {
			vm.showShare = !vm.showShare;
			console.log("clicked", vm.showShare);
		}

		vm.closeFeedback = function() {
			vm.showFeedback = false;
		}

	    // FORMULA MODAL
		vm.showFormula = false;
		vm.toggleFormula = function() {
			vm.showFormula = !vm.showFormula;
			console.log("clicked", vm.showFormula);
		}

		vm.closeFormula = function() {
			vm.showFormula = false;
		}

		// SIMULATION MODAL
		vm.showSimulate = false;
		vm.toggleSimulate = function() {
			vm.showSimulate = !vm.showSimulate;
		}

		vm.closeSimulate = function() {
			vm.showSimulate = false;
		}



      angular.element(document).ready(function () {
          $(".top-bar").addClass("animated visible slideInDown");
          setTimeout('$("#lgu-banner").addClass("animated visible fadeInDown")', 100);
          setTimeout('$(".lgu-info").addClass("animated visible fadeInDown")', 400);
          setTimeout('$(".perf-poor").addClass("animated visible fadeInRight")', 600);
          setTimeout('$(".perf-vgood").addClass("animated visible fadeInRight")', 800);
          setTimeout('$(".perf-good").addClass("animated visible fadeInRight")', 1000);

	        $(".show-fields button").click(function() {
	          $('.fields').toggleClass('fields-hidden');
	          $('.show-fields').toggleClass('close');
	        });


        $("#compare").click(function() {
          $('.compare-content').toggleClass('show-compare');
          $('.indicator-compare-container').toggleClass('show-compare');

            // var $this = $(this);

            // $this.toggleClass("close-compare");

            // if ($this.hasClass("close-compare")) {
            //     $this.html("CLOSE COMPARE");
            // } else {
            //     $this.html("COMPARE");
            // }

        });
      });


	}
})()