(function(){
	'use strict';

	angular.module('PORTAL.directives')

	.directive('toggleSide', function() {
		return {
			restrict: 'A',
			link: function(scope, elem, attrs) {
				elem.on('click',function(e){
					e.preventDefault();
					e.stopPropagation();
					// var self = $(this);
					// $(this).removeClass('wala');
					// console.log('button clicked!');
					// console.log(attrs);
					$(".wrapper").toggleClass("collapse");

				})
			}
		}
	})

	.directive('toggleCompare', function() {
		return {
			restrict: 'C',
			link: function(scope, elem, attrs) {
				elem.on('click',function(e){
					e.preventDefault();
					e.stopPropagation();
					// var self = $(this);
					// $(this).removeClass('wala');
					// console.log('button clicked!');
					// console.log(attrs);
					$(".indicator-compare-container").toggleClass("show-compare");


				})
			}
		}
	})


	.directive('modal', function() {
	  return {
	    restrict: 'A',
	    scope: {
	      show: '='
	    },
	    replace: true, // Replace with the template below
	    transclude: true, // we want to insert custom content inside the directive
	    link: function(scope, element, attrs) {
	      scope.hideModal = function() {
	        scope.show = false;
	      };
	    },
	    template: "<div class='ng-modal' ng-show='show'>"+
	                "<div class='reveal-modal' ng-show='show'>"+
	                  "<a class='close-reveal-modal' ng-click='hideModal();'>&#215;</a>"+
	                  "<div ng-transclude></div>"+
	                "</div>"+
	                "<div class='reveal-modal-bg' ng-click='hideModal()'></div>"+
	              "</div>"
	  };
	});


})()