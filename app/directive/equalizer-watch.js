(function(){
	'use strict';

	angular.module('PORTAL.directives')

	.directive('equalizerWatch', function() {
		return {
			restrict: 'C',
			link: function(scope, elem, attrs) {
			
			var maxHeight = 0;

				$(".equalizer-watch").each(function(){
				   if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
				});

				$(".equalizer-watch").height(maxHeight);


			}
		}
	});

})()