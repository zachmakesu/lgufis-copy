(function(){
  'use strict';

  angular.module('PORTAL.contactUs')

  .config(['$stateProvider', 'LOCATION',routeFunc])

  function routeFunc($stateProvider, LOCATION) {

    $stateProvider

    .state("contact-us", {
      url: "/contact-us",
      templateUrl: "app/contact-us/index.html",
      controller: "ContactUsCtrl",
      title: 'Contact Us'
    })


  }

})()